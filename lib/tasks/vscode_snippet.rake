namespace :qismo do
  namespace :vscode_snippet do
    desc "Generate Qismo VSCode snippet"
    task :create do
      source_file = Pathname.new(Gem.loaded_specs["qismo"]&.full_gem_path).join("vscode-snippet", "qismo.code-snippets")
      destination_folder = Rails.root.join(".vscode")

      FileUtils.mkdir(destination_folder.to_s) unless Dir.exist?(destination_folder.to_s)
      FileUtils.cp(source_file.to_s, destination_folder.to_s)

      puts "Successfully created Qismo vscode snippet"
    end

    desc "Delete Qismo VSCode snippet"
    task :delete do
      destination_file = Rails.root.join(".vscode", "qismo.code-snippets")
      FileUtils.rm_f(destination_file)

      puts "Successfully deleted Qismo vscode snippet"
    end
  end
end
