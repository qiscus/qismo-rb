module Qismo
  # Objects module to handle objectification of incoming webhook request
  #
  module WebhookRequests
    # CAA webhook object
    #
    class OnAgentAllocationNeeded < Qismo::Object
      # Candidate agent object
      #
      # @!attribute [r] avatar_url
      #   @return [String]
      # @!attribute [r] created_at
      #   @return [String]
      # @!attribute [r] email
      #   @return [String]
      # @!attribute [r] force_offline
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] is_available
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] is_verified
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] last_login
      #   @return [String]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] sdk_email
      #   @return [String]
      # @!attribute [r] sdk_key
      #   @return [String]
      # @!attribute [r] updated_at
      #   @return [String]
      class CandidateAgent < Qismo::Object
        attribute? :avatar_url, Types::String.optional
        attribute? :created_at, Types::String.optional
        attribute? :email, Types::String.optional
        attribute? :force_offline, Types::Bool.optional
        attribute? :id, Types::Int.optional
        attribute? :is_available, Types::Bool.optional
        attribute? :is_verified, Types::Bool.optional
        attribute? :last_login, Types::String.optional
        attribute? :name, Types::String.optional
        attribute? :sdk_email, Types::String.optional
        attribute? :sdk_key, Types::String.optional
        attribute? :updated_at, Types::String.optional
      end

      # @!attribute [r] app_id
      #   @return [String]
      # @!attribute [r] avatar_url
      #   @return [String]
      # @!attribute [r] candidate_agent
      #   @return [CandidateAgent]
      # @!attribute [r] email
      #   @return [String]
      # @!attribute [r] extras
      #   @return [String]
      # @!attribute [r] is_new_session
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] is_resolved
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] room_id
      #   @return [String]
      # @!attribute [r] source
      #   @return [String]
      attribute? :app_id, Types::String.optional
      attribute? :avatar_url, Types::String.optional
      attribute? :candidate_agent, CandidateAgent
      attribute? :email, Types::String.optional
      attribute? :extras, Types::String.optional
      attribute? :is_new_session, Types::Bool.optional
      attribute? :is_resolved, Types::Bool.optional
      attribute? :name, Types::String.optional
      attribute? :room_id, Types::String.optional
      attribute? :source, Types::String.optional
    end
  end
end
