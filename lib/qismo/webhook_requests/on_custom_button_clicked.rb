module Qismo
  module WebhookRequests
    # Custom button webhook object
    #
    # @!attribute [r] additional_info
    #   @return [Array<AdditionalInfo>]
    # @!attribute [r] agent
    #   @return [Agent]
    # @!attribute [r] channel_id
    #   @return [Integer]
    # @!attribute [r] channel_name
    #   @return [String]
    # @!attribute [r] channel_type
    #   @return [String]
    # @!attribute [r] customer
    #   @return [Customer]
    # @!attribute [r] notes
    #   @return [String]
    # @!attribute [r] room_id
    #   @return [Integer]
    # @!attribute [r] tag
    #   @return [Array<Tag>]
    class OnCustomButtonClicked < Qismo::Object
      # Custom button webhook additional info object
      #
      # @!attribute [r] key
      #   @return [String]
      # @!attribute [r] value
      #   @return [String,Integer,TrueClass,FalseClass]
      class AdditionalInfo < Qismo::Object
        attribute? :key, Types::String.optional
        attribute? :value, (Types::String.optional | Types::Int.optional | Types::Params::Bool.optional)
      end

      # Custom button webhook agent object
      #
      # @!attribute [r] email
      #   @return [String]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] type
      #   @return [String]
      class Agent < Qismo::Object
        attribute? :email, Types::String.optional
        attribute? :name, Types::String.optional
        attribute? :type, Types::String.optional
      end

      # Custom webhook customer object
      #
      # @!attribute [r] avatar
      #   @return [String]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] user_id
      #   @return [String]
      class Customer < Qismo::Object
        attribute? :avatar, Types::String.optional
        attribute? :name, Types::String.optional
        attribute? :user_id, Types::String.optional
      end

      # Custom button webhook tag object
      #
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] created_at
      #   @return [String]
      # @!attribute [r] updated_at
      #   @return [String]
      # @!attribute [r] room_tag_created_at
      #   @return [String]
      class Tag < Qismo::Object
        attribute? :id, Types::Int.optional
        attribute? :name, Types::String.optional
        attribute? :created_at, Types::String.optional
        attribute? :updated_at, Types::String.optional
        attribute? :room_tag_created, Types::String.optional
      end

      attribute? :additional_info, Types.Array(AdditionalInfo.optional).optional
      attribute? :agent, Agent.optional
      attribute? :channel_id, Types::Int.optional
      attribute? :channel_name, Types::String.optional
      attribute? :channel_type, Types::String.optional
      attribute? :customer, Customer.optional
      attribute? :notes, Types::String.optional
      attribute? :room_id, Types::Int.optional
      attribute? :tag, Types.Array(Tag.optional).optional
    end
  end
end
