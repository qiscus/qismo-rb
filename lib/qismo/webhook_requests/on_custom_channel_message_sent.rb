module Qismo
  module WebhookRequests
    # Custom channel webhook object
    #
    # @!attribute [r] payload
    #   @return [Payload]
    class OnCustomChannelMessageSent < Qismo::Object
      # Custom channel webhook from object
      #
      # @!attribute [r] avatar_url
      #   @return [String]
      # @!attribute [r] email
      #   @return [String]
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] name
      #   @return [String]
      class From < Qismo::Object
        attribute? :avatar_url, Types::String.optional
        attribute? :email, Types::String.optional
        attribute? :id, Types::Int.optional
        attribute? :name, Types::String.optional
      end

      # Custom channel webhook message object
      #
      # @!attribute [r] comment_before_id
      #   @return [Integer]
      # @!attribute [r] created_at
      #   @return [String]
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] payload
      #   @return [Hash]
      # @!attribute [r] text
      #   @return [String]
      # @!attribute [r] timestamp
      #   @return [String]
      # @!attribute [r] type
      #   @return [String]
      # @!attribute [r] unique_temp_id
      #   @return [String]
      # @!attribute [r] unix_nano_timestamp
      #   @return [String]
      # @!attribute [r] unix_timestamp
      #   @return [String]
      class Message < Qismo::Object
        attribute? :comment_before_id, Types::Int.optional
        attribute? :created_at, Types::String.optional
        attribute? :id, Types::Int.optional
        attribute? :payload, Types::Hash.optional
        attribute? :text, Types::String.optional
        attribute? :timestamp, Types::String.optional
        attribute? :type, Types::String.optional
        attribute? :unique_temp_id, Types::String.optional
        attribute? :unix_nano_timestamp, Types::String.optional
        attribute? :unix_timestamp, Types::String.optional
      end

      # Custom channel webhook participant object
      #
      # @!attribute [r] email
      #   @return [String]
      class Participant < Qismo::Object
        attribute? :email, Types::String.optional
      end

      # Custom channel webhook room object
      #
      # @!attribute [r] id
      #   @return [String]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] options
      #   @return [String]
      # @!attribute [r] participants
      #   @return [Array<Participant>]
      # @!attribute [r] room_avatar
      #   @return [String]
      class Room < Qismo::Object
        attribute? :id, Types::String.optional
        attribute? :name, Types::String.optional
        attribute? :options, Types::String.optional
        attribute? :participants, Types.Array(Participant.optional).optional
        attribute? :room_avatar, Types::String.optional
      end

      # Custom channel webhook payload object
      #
      # @!attribute [r] from
      #   @return [From]
      # @!attribute [r] message
      #   @return [Message]
      # @!attribute [r] room
      #   @return [Room]
      # @!attribute [r] type
      #   @return [String]
      class Payload < Qismo::Object
        attribute? :from, From.optional
        attribute? :message, Message.optional
        attribute? :room, Room.optional
      end

      attribute? :payload, Payload.optional
    end
  end
end
