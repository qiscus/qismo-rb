module Qismo
  module WebhookRequests
    # Room resolve webhook object
    #
    # @!attribute [r] customer
    #   @return [Customer]
    # @!attribute [r] resolved_by
    #   @return [ResolvedBy]
    # @!attribute [r] service
    #   @return [Service]
    class OnRoomResolved < Qismo::Object
      # Room resolve webhook additional info object
      #
      # @!attribute [r] key
      #   @return [String]
      # @!attribute [r] value
      #   @return [String,Integer,TrueClass,FalseClass]
      class AdditionalInfo < Qismo::Object
        attribute? :key, Types::String.optional
        attribute? :value, (Types::String.optional | Types::Int.optional | Types::Params::Bool.optional)
      end

      # Room resolve webhook customer object
      #
      # @!attribute [r] additional_info
      #   @return [Array<AdditionalInfo>]
      # @!attribute [r] avatar
      #   @return [String]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] user_id
      #   @return [String]
      class Customer < Qismo::Object
        attribute? :additional_info, Types.Array(AdditionalInfo.optional).optional
        attribute? :avatar, Types::String.optional
        attribute? :name, Types::String.optional
        attribute? :user_id, Types::String.optional
      end

      # Room resolve webhook resolved by object
      #
      # @!attribute [r] email
      #   @return [String]
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] is_available
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] type
      #   @return [String]
      class ResolvedBy < Qismo::Object
        attribute? :email, Types::String.optional
        attribute? :id, Types::Int.optional
        attribute? :is_available, Types::Bool.optional
        attribute? :name, Types::String.optional
        attribute? :type, Types::String.optional
      end

      # Room resolve webhook service object
      #
      # @!attribute [r] first_comment_id
      #   @return [String]
      # @!attribute [r] last_comment_id
      #   @return [String]
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] is_resolved
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] notes
      #   @return [String]
      # @!attribute [r] room_id
      #   @return [String]
      # @!attribute [r] source
      #   @return [String]
      class Service < Qismo::Object
        attribute? :first_comment_id, Types::String.optional
        attribute? :last_comment_id, Types::String.optional
        attribute? :id, Types::Int.optional
        attribute? :is_resolved, Types::Bool.optional
        attribute? :notes, Types::String.optional
        attribute? :room_id, Types::String.optional
        attribute? :source, Types::String.optional
      end

      attribute? :customer, Customer
      attribute? :resolved_by, ResolvedBy
      attribute? :service, Service
    end
  end
end
