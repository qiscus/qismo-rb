module Qismo
  module WebhookRequests
    # New session webhook object
    #
    # @!attribute [r] is_new_session
    #   @return [TrueClass,FalseClass]
    # @!attribute [r] payload
    #   @return [Payload]
    # @!attribute [r] room_log
    #   @return [RoomLog]
    class OnNewSessionInitiated < Qismo::Object
      # New session webhook from object
      #
      # @!attribute [r] avatar_url
      #   @return [String]
      # @!attribute [r] email
      #   @return [String]
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] name
      #   @return [String]
      class From < Qismo::Object
        attribute? :avatar_url, Types::String.optional
        attribute? :email, Types::String.optional
        attribute? :id, Types::Int.optional
        attribute? :name, Types::String.optional
      end

      # New session webhook message object
      #
      # @!attribute [r] comment_before_id
      #   @return [Integer]
      # @!attribute [r] created_at
      #   @return [String]
      # @!attribute [r] id
      #   @return [Integer]
      # @!attribute [r] payload
      #   @return [Hash]
      # @!attribute [r] text
      #   @return [String]
      # @!attribute [r] timestamp
      #   @return [String]
      # @!attribute [r] type
      #   @return [String]
      # @!attribute [r] unique_temp_id
      #   @return [String]
      # @!attribute [r] unix_nano_timestamp
      #   @return [String]
      # @!attribute [r] unix_timestamp
      #   @return [String]
      class Message < Qismo::Object
        attribute? :comment_before_id, Types::Int.optional
        attribute? :created_at, Types::String.optional
        attribute? :id, Types::Int.optional
        attribute? :payload, Types::Hash.optional
        attribute? :text, Types::String.optional
        attribute? :timestamp, Types::String.optional
        attribute? :type, Types::String.optional
        attribute? :unique_temp_id, Types::String.optional
        attribute? :unix_nano_timestamp, Types::String.optional
        attribute? :unix_timestamp, Types::String.optional
      end

      # New session webhook participant object
      #
      # @!attribute [r] email
      #   @return [String]
      class Participant < Qismo::Object
        attribute? :email, Types::String.optional
      end

      # New session webhook room object
      #
      # @!attribute [r] id
      #   @return [String]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] options
      #   @return [String]
      # @!attribute [r] participants
      #   @return [Array<Participant>]
      # @!attribute [r] room_avatar
      #   @return [String]
      class Room < Qismo::Object
        attribute? :id, Types::String.optional
        attribute? :name, Types::String.optional
        attribute? :options, Types::String.optional
        attribute? :participants, Types.Array(Participant.optional).optional
        attribute? :room_avatar, Types::String.optional
      end

      # New session webhook message payload object
      #
      # @!attribute [r] from
      #   @return [From]
      # @!attribute [r] message
      #   @return [Message]
      # @!attribute [r] room
      #   @return [Room]
      # @!attribute [r] type
      #   @return [String]
      class Payload < Qismo::Object
        attribute? :from, From.optional
        attribute? :message, Message.optional
        attribute? :room, Room.optional
      end

      # New session webhook user property object
      #
      # @!attribute [r] key
      #   @return [String]
      # @!attribute [r] value
      #   @return [String,Integer,TrueClass,FalseClass]
      class UserProperty < Qismo::Object
        attribute? :key, Types::String.optional
        attribute? :value, (Types::String.optional | Types::Int.optional | Types::Params::Bool.optional)
      end

      # New session webhook additional extras object
      #
      # @!attribute [r] timezone_offset
      #   @return [String]
      class AdditionalExtras < Qismo::Object
        attribute? :timezone_offset, Types::String
      end

      # New session webhook extras object
      #
      # @!attribute [r] additional_extras
      #   @return [AdditionalExtras]
      # @!attribute [r] notes
      #   @return [String]
      # @!attribute [r] timezone_offset
      #   @return [String]
      # @!attribute [r] user_properties
      #   @return [Array<UserProperty>]
      class Extras < Qismo::Object
        attribute? :additional_extras, AdditionalExtras.optional
        attribute? :notes, Types::String
        attribute? :timezone_offset, Types::String
        attribute? :user_properties, Types.Array(UserProperty.optional).optional
      end

      # New session webhook room log object
      #
      # @!attribute [r] channel_id
      #   @return [Integer]
      # @!attribute [r] created_at
      #   @return [String]
      # @!attribute [r] extras
      #   @return [String]
      # @!attribute [r] has_no_message
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] is_waiting
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] name
      #   @return [String]
      # @!attribute [r] resolved
      #   @return [TrueClass,FalseClass]
      # @!attribute [r] room_badge
      #   @return [String]
      # @!attribute [r] room_id
      #   @return [String]
      # @!attribute [r] source
      #   @return [String]
      # @!attribute [r] user_avatar_url
      #   @return [String]
      # @!attribute [r] user_id
      #   @return [String]
      class RoomLog < Qismo::Object
        attribute? :channel_id, Types::Int
        attribute? :created_at, Types::String
        attribute? :extras, Extras
        attribute? :has_no_message, Types::Bool.optional
        attribute? :is_waiting, Types::Nil
        attribute? :name, Types::String
        attribute? :resolved, Types::Bool.optional
        attribute? :room_badge, Types::String
        attribute? :room_id, Types::String
        attribute? :source, Types::String
        attribute? :user_avatar_url, Types::String
        attribute? :user_id, Types::String
      end

      attribute? :is_new_session, Types::Bool.optional
      attribute? :payload, Payload
      attribute? :room_log, RoomLog
    end
  end
end
