module Qismo
  # Type specs module. Inherited from dry-types
  #
  module Types
    include Dry.Types()

    # @return [Integer]
    Int = Params::Integer

    # @return [String]
    String = Coercible::String

    # @return [TrueClass,FalseClass]
    Bool = Params::Bool

    # @return [Hash]
    Hash = Params::Hash
  end
end
