require "qismo/object"

module Qismo
  module Objects
    # Customer room object
    #
    class CustomerRoom < Qismo::Object
      # @!attribute [r] channel_id
      #   @return [Integer]
      attribute? :channel_id, Types::Int.optional

      # @!attribute [r] contact_id
      #   @return [Integer]
      attribute? :contact_id, Types::Int.optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] is_handled_by_bot
      #   @return [TrueClass,FalseClass]
      attribute? :is_handled_by_bot, Types::Bool.optional

      # @!attribute [r] is_resolved
      #   @return [String]
      attribute? :is_resolved, Types::Bool.optional

      # @!attribute [r] is_waiting
      #   @return [TrueClass,FalseClass]
      attribute? :is_waiting, Types::Bool.optional

      # @!attribute [r] last_comment_sender
      #   @return [String]
      attribute? :last_comment_sender, String

      # @!attribute [r] last_comment_sender_type
      #   @return [String]
      attribute? :last_comment_sender_type, Types::String.optional

      # @!attribute [r] last_comment_text
      #   @return [String]
      attribute? :last_comment_text, Types::String.optional

      # @!attribute [r] last_comment_timestamp
      #   @return [String]
      attribute? :last_comment_timestamp, Types::String.optional

      # @!attribute [r] last_customer_comment_text
      #   @return [String]
      attribute? :last_customer_comment_text, Types::String.optional

      # @!attribute [r] last_customer_timestamp
      #   @return [String]
      attribute? :last_customer_timestamp, Types::String.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] room_badge
      #   @return [String]
      attribute? :room_badge, Types::String.optional

      # @!attribute [r] room_id
      #   @return [String]
      attribute? :room_id, Types::Int.optional

      # @!attribute [r] room_type
      #   @return [String]
      attribute? :room_type, Types::String.optional

      # @!attribute [r] source
      #   @return [String]
      attribute? :source, Types::String.optional

      # @!attribute [r] user_avatar_url
      #   @return [String]
      attribute? :user_avatar_url, Types::String.optional

      # @!attribute [r] user_id
      #   @return [String]
      attribute? :user_id, Types::String.optional
    end
  end
end
