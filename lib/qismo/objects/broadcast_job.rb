module Qismo
  module Objects
    # Broadcast job object
    #
    class BroadcastJob < Qismo::Object
      # Broadcast job user object
      #
      class User < Qismo::Object
        # @!attribute [r] name
        #   @return [String]
        attribute? :name, Types::String.optional
      end

      # @!attribute [r] user
      #   @return [Qismo::Objects::BroadcastJob::User]
      attribute? :user, User.optional

      # @!attribute [r] broadcast_job_id
      #   @return [Integer]
      attribute? :broadcast_job_id, Types::Int.optional

      # @!attribute [r] broadcast_logs
      #   @return [Array<Qismo::Objects::BroadcastLog>]
      attribute? :broadcast_logs, Types.Array(Qismo::Objects::BroadcastLog.optional).optional

      # @!attribute [r] language
      #   @return [String]
      attribute? :language, Types::String.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] namespace
      #   @return [String]
      attribute? :namespace, Types::String.optional

      # @!attribute [r] canceled_at
      #   @return [String]
      attribute? :canceled_at, Types::String.optional

      # @!attribute [r] canceled_by
      #   @return [Integer]
      attribute? :canceled_by, Types::Int.optional

      # @!attribute [r] channel_id
      #   @return [Integer]
      attribute? :channel_id, Types::Int.optional

      # @!attribute [r] channel_name
      #   @return [String]
      attribute? :channel_name, Types::String.optional

      # @!attribute [r] created_at
      #   @return [String]
      attribute? :created_at, Types::String.optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int | Types::String

      # @!attribute [r] started_at
      #   @return [String]
      attribute? :started_at, Types::String.optional

      # @!attribute [r] status
      #   @return [String]
      attribute? :status, Types::String.optional

      # @!attribute [r] template_detail_id
      #   @return [Integer]
      attribute? :template_detail_id, Types::Int.optional

      # @!attribute [r] template_id
      #   @return [Integer]
      attribute? :template_id, Types::Int.optional

      # @!attribute [r] template_name
      #   @return [String]
      attribute? :template_name, Types::String.optional

      # @!attribute [r] total_failed
      #   @return [Integer]
      attribute? :total_failed, Types::Int.optional

      # @!attribute [r] total_read
      #   @return [Integer]
      attribute? :total_read, Types::Int.optional

      # @!attribute [r] total_received
      #   @return [Integer]
      attribute? :total_received, Types::Int.optional

      # @!attribute [r] total_recipient
      #   @return [Integer]
      attribute? :total_recipient, Types::Int.optional
    end
  end
end
