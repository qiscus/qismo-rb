module Qismo
  module Objects
    # Tag object
    #
    class Tag < Qismo::Object
      # @!attribute [r] created_at
      #   @return [String]
      attribute? :created_at, Types::String.optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] room_tag_created_at
      #   @return [String]
      attribute? :room_tag_created, Types::String.optional

      # @!attribute [r] updated_at
      #   @return [String]
      attribute? :updated_at, Types::String.optional
    end
  end
end
