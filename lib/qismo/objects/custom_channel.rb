module Qismo
  module Objects
    # Custom channel object
    #
    class CustomChannel < Qismo::Object
      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] webhook_url
      #   @return [String]
      attribute? :webhook_url, Types::String.optional

      # @!attribute [r] logo_url
      #   @return [String]
      attribute? :logo_url, Types::String.optional

      # @!attribute [r] identifier_key
      #   @return [String]
      attribute? :identifier_key, Types::String.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] is_active
      #   @return [TrueClass,FalseClass]
      attribute? :is_active, Types::Bool.optional

      # @!attribute [r] use_channel_responder
      #   @return [TrueClass,FalseClass]
      attribute? :use_channel_responder, Types::Bool.optional
    end
  end
end
