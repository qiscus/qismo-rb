module Qismo
  module Objects
    # Fb channel object
    #
    class FbChannel < Qismo::Object
      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] is_active
      #   @return [TrueClass,FalseClass]
      attribute? :is_active, Types::Bool.optional

      # @!attribute [r] profile_name
      #   @return [String]
      attribute? :profile_name, Types::String.optional

      # @!attribute [r] page_id
      #   @return [String]
      attribute? :page_id, Types::String.optional

      # @!attribute [r] badge_url
      #   @return [String]
      attribute? :badge_url, Types::String.optional

      # @!attribute [r] use_channel_responder
      #   @return [TrueClass,FalseClass]
      attribute :use_channel_responder, Types::Bool.optional
    end
  end
end
