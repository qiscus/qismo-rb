module Qismo
  module Objects
    # HSM template object
    #
    class HsmTemplate < Qismo::Object
      # Hsm detail object
      #
      class HsmDetail < Qismo::Object
        # Hsm detail button object
        #
        class Button < Qismo::Object
          # @!attribute [r] text
          #   @return [String]
          attribute? :text, Types::String.optional

          # @!attribute [r] type
          #   @return [String]
          attribute? :type, Types::String.optional

          # @!attribute [r] url
          #   @return [String]
          attribute? :url, Types::String.optional

          # @!attribute [r] phone_number
          #   @return [String]
          attribute? :phone_number, Types::String.optional
        end

        # @!attribute [r] approval_status
        #   @return [Integer]
        attribute? :approval_status, Types::Int.optional

        # @!attribute [r] buttons
        #   @return [Array<Qismo::Objects::HsmInfo::Button>]
        attribute? :buttons, Types.Array(Button.optional).optional

        # @!attribute [r] content
        #   @return [String]
        attribute? :content, Types::String.optional

        # @!attribute [r] footer
        #   @return [String]
        attribute? :footer, Types::String.optional

        # @!attribute [r] header_content
        #   @return [String]
        attribute? :header_content, Types::String.optional

        # @!attribute [r] header_default_value
        #   @return [String]
        attribute? :header_default_value, Types::String.optional

        # @!attribute [r] header_type
        #   @return [String]
        attribute? :header_type, Types::String.optional

        # @!attribute [r] id
        #   @return [Integer]
        attribute? :id, Types::Int.optional

        # @!attribute [r] language
        #   @return [String]
        attribute? :language, Types::String.optional

        # @!attribute [r] number_of_arguments
        #   @return [Integer]
        attribute? :number_of_arguments, Types::Int.optional

        # @!attribute [r] rejection_reason
        #   @return [String]
        attribute? :rejection_reason, Types::String.optional

        # @!attribute [r] tested
        #   @return [TrueClass,FalseClass]
        attribute? :tested, Types::Bool.optional
      end

      # @!attribute [r] category
      #   @return [String]
      attribute? :category, Types::String.optional

      # @!attribute [r] channel_id
      #   @return [Integer]
      attribute? :channel_id, Types::Int.optional

      # @!attribute [r] channel_name
      #   @return [String]
      attribute? :channel_name, Types::String.optional

      # @!attribute [r] hsm_details
      #   @return [Array<Qismo::Objects::HsmInfo::HsmDetail>]
      attribute? :hsm_details, Types.Array(HsmDetail.optional).optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] namespace
      #   @return [String]
      attribute? :namespace, Types::String.optional

      # @!attribute [r] type
      #   @return [Integer]
      attribute? :type, Types::Int.optional
    end
  end
end
