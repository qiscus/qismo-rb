module Qismo
  module Objects
    # IG channel object
    #
    class IgChannel < Qismo::Object
      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] is_active
      #   @return [TrueClass,FalseClass]
      attribute? :is_active, Types::Bool.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] page_id
      #   @return [String]
      attribute? :page_id, Types::String.optional

      # @!attribute [r] badge_url
      #   @return [String]
      attribute? :badge_url, Types::String.optional

      # @!attribute [r] use_channel_responder
      #   @return [TrueClass,FalseClass]
      attribute? :use_channel_responder, Types::Bool.optional

      # @!attribute [r] ig_id
      #   @return [String]
      attribute? :ig_id, Types::String.optional

      # @!attribute [r] private_replies_enabled
      #   @return [TrueClass,FalseClass]
      attribute? :private_replies_enabled, Types::Bool.optional

      # @!attribute [r] private_replies_text
      #   @return [String]
      attribute? :private_replies_text, Types::String.optional
    end
  end
end
