module Qismo
  module Objects
    # WA channel object
    #
    class WaChannel < Qismo::Object
      # @!attribute [r] allow_intl_hsm
      #   @return [TrueClass,FalseClass]
      attribute? :allow_intl_hsm, Types::Bool.optional

      # @!attribute [r] alter_app_id
      #   @return [TrueClass,FalseClass]
      attribute? :alter_app_id, Types::Bool.optional

      # @!attribute [r] badge_url
      #   @return [String]
      attribute? :badge_url, Types::String.optional

      # @!attribute [r] base_url
      #   @return [String]
      attribute? :base_url, Types::String.optional

      # @!attribute [r] business_id
      #   @return [String]
      attribute? :business_id, Types::String.optional

      # @!attribute [r] business_verification_status
      #   @return [String]
      attribute? :business_verification_status, Types::String.optional

      # @!attribute [r] created_at
      #   @return [String]
      attribute? :created_at, Types::String.optional

      # @!attribute [r] forward_enabled
      #   @return [TrueClass,FalseClass]
      attribute? :forward_enabled, Types::Bool.optional

      # @!attribute [r] forward_url
      #   @return [String]
      attribute? :forward_url, Types::String.optional

      # @!attribute [r] hsm_24_enabled
      #   @return [TrueClass,FalseClass]
      attribute? :hsm_24_enabled, Types::Bool.optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] is_active
      #   @return [TrueClass,FalseClass]
      attribute? :is_active, Types::Bool.optional

      # @!attribute [r] is_auto_responder_enabled
      #   @return [TrueClass,FalseClass]
      attribute? :is_auto_responder_enabled, Types::Bool.optional

      # @!attribute [r] is_bot_enabled
      #   @return [TrueClass,FalseClass]
      attribute? :is_bot_enabled, Types::Bool.optional

      # @!attribute [r] is_on_cloud
      #   @return [TrueClass,FalseClass]
      attribute? :is_on_cloud, Types::Bool.optional

      # @!attribute [r] is_ssl_enabled
      #   @return [TrueClass,FalseClass]
      attribute? :is_ssl_enabled, Types::Bool.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] on_sync
      #   @return [TrueClass,FalseClass]
      attribute? :on_sync, Types::Bool.optional

      # @!attribute [r] phone_number
      #   @return [String]
      attribute? :phone_number, Types::String.optional

      # @!attribute [r] phone_number_id
      #   @return [String]
      attribute? :phone_number_id, Types::String.optional

      # @!attribute [r] phone_number_status
      #   @return [String]
      attribute? :phone_number_status, Types::String.optional

      # @!attribute [r] pin
      #   @return [String]
      attribute? :pin, Types::String.optional

      # @!attribute [r] platform
      #   @return [String]
      attribute? :platform, Types::String.optional

      # @!attribute [r] read_enabled
      #   @return [TrueClass,FalseClass]
      attribute? :read_enabled, Types::Bool.optional

      # @!attribute [r] updated_at
      #   @return [String]
      attribute? :updated_at, Types::String.optional

      # @!attribute [r] use_channel_responder
      #   @return [TrueClass,FalseClass]
      attribute? :use_channel_responder, Types::Bool.optional
    end
  end
end
