module Qismo
  # Objects module to handle schema for API response
  #
  module Objects
    # Agent service object
    #
    class AgentService < Qismo::Object
      # @!attribute [r] created_at
      #   @return [String]
      attribute? :created_at, Types::String.optional

      # @!attribute [r] first_comment_id
      #   @return [String]
      attribute? :first_comment_id, Types::String.optional

      # @!attribute [r] first_comment_timestamp
      #   @return [String]
      attribute? :first_comment_timestamp, Types::String.optional

      # @!attribute [r] is_resolved
      #   @return [TrueClass,FalseClass]
      attribute? :is_resolved, Types::Bool.optional

      # @!attribute [r] last_comment_id
      #   @return [String]
      attribute? :last_comment_id, Types::String.optional

      # @!attribute [r] notes
      #   @return [String]
      attribute? :notes, Types::String.optional

      # @!attribute [r] resolved_at
      #   @return [String]
      attribute? :resolved_at, Types::String.optional

      # @!attribute [r] retrieved_at
      #   @return [String]
      attribute? :retrieved_at, Types::String.optional

      # @!attribute [r] room_id
      #   @return [Integer]
      attribute? :room_id, Types::String.optional

      # @!attribute [r] updated_at
      #   @return [String]
      attribute? :updated_at, Types::String.optional

      # @!attribute [r] user_id
      #   @return [Integer]
      attribute? :user_id, Types::Int.optional
    end
  end
end
