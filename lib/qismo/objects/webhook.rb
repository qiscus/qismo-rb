module Qismo
  module Objects
    # Webhook object
    #
    class Webhook < Qismo::Object
      # @!attribute [r] url
      #   @return [String]
      attribute? :url, Types::String.optional

      # @!attribute [r] enabled
      #   @return [TrueClass,FalseClass]
      attribute? :enabled, Types::Bool.optional
    end
  end
end
