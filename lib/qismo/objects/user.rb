module Qismo
  module Objects
    # User object
    #
    class User < Qismo::Object
      # User channel object
      #
      class Channel < Qismo::Object
        # @!attribute [r] id
        #   @return [Integer]
        attribute? :id, Types::Int.optional

        # @!attribute [r] name
        #   @return [String]
        attribute? :name, Types::String.optional
      end

      # User role object
      #
      class Role < Qismo::Object
        # @!attribute [r] id
        #   @return [Integer]
        attribute? :id, Types::Int.optional

        # @!attribute [r] name
        #   @return [String]
        attribute? :name, Types::String.optional
      end

      # @!attribute [r] user_channels
      #   @return [Array<Qismo::Objects::User::Channel>]
      attribute? :user_channels, Types.Array(Channel.optional).optional

      # @!attribute [r] user_roles
      #   @return [Array<Qismo::Objects::User::Role>]
      attribute? :user_roles, Types.Array(Role.optional).optional

      # @!attribute [r] avatar_url
      #   @return [String]
      attribute? :avatar_url, Types::String.optional

      # @!attribute [r] created_at
      #   @return [String]
      attribute? :created_at, Types::String.optional

      # @!attribute [r] email
      #   @return [String]
      attribute? :email, Types::String.optional

      # @!attribute [r] force_offline
      #   @return [TrueClass,FalseClass]
      attribute? :force_offline, Types::Bool.optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] is_available
      #   @return [TrueClass,FalseClass]
      attribute? :is_available, Types::Bool.optional

      # @!attribute [r] is_verified
      #   @return [TrueClass,FalseClass]
      attribute? :is_verified, Types::Bool.optional

      # @!attribute [r] last_login
      #   @return [String]
      attribute? :last_login, Types::String.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] sdk_email
      #   @return [String]
      attribute? :sdk_email, Types::String.optional

      # @!attribute [r] sdk_key
      #   @return [String]
      attribute? :sdk_key, Types::String.optional

      # @!attribute [r] type
      #   @return [String]
      attribute? :type, Types::Int.optional

      # @!attribute [r] type_as_string
      #   @return [String]
      attribute? :type_as_string, Types::String.optional

      # @!attribute [r] updated_at
      #   @return [String]
      attribute? :updated_at, Types::String.optional

      # @!attribute [r] current_customer_count
      #   @return [Integer]
      attribute? :current_customer_count, Types::Int.optional
    end
  end
end
