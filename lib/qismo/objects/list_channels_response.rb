module Qismo
  module Objects
    # List channels response object
    #
    class ListChannelsResponse < Qismo::Object
      # @!attribute [r] custom_channels
      #   @return [Array<Qismo::Objects::CustomChannel>]
      attribute? :custom_channels, Types.Array(Qismo::Objects::CustomChannel.optional).optional

      # @!attribute [r] fb_channels
      #   @return [Array<Qismo::Objects::FbChannel>]
      attribute? :fb_channels, Types.Array(Qismo::Objects::FbChannel.optional).optional

      # @!attribute [r] ig_channels
      #   @return [Array<Qismo::Objects::IgChannel>]
      attribute? :ig_channels, Types.Array(Qismo::Objects::IgChannel.optional).optional

      # @!attribute [r] line_channels
      #   @return [Array<Qismo::Objects::LineChannel>]
      attribute? :line_channels, Types.Array(Qismo::Objects::LineChannel.optional).optional

      # @!attribute [r] qiscus_channels
      #   @return [Array<Qismo::Objects::QiscusChannel>]
      attribute? :qiscus_channels, Types.Array(Qismo::Objects::QiscusChannel.optional).optional

      # @!attribute [r] telegram_channels
      #   @return [Array<Qismo::Objects::TelegramChannel>]
      attribute? :telegram_channels, Types.Array(Qismo::Objects::TelegramChannel.optional).optional

      # @!attribute [r] wa_channels
      #   @return [Array<Qismo::Objects::WaChannel>]
      attribute? :wa_channels, Types.Array(Qismo::Objects::WaChannel.optional).optional

      # @!attribute [r] waca_channels
      #   @return [Array<Qismo::Objects::WacaChannel>]
      attribute? :waca_channels, Types.Array(Qismo::Objects::WacaChannel.optional).optional
    end
  end
end
