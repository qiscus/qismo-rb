module Qismo
  module Objects
    # Waca channel object
    #
    class WacaChannel < Qismo::Object
      # @!attribute [r] badge_url
      #   @return [String]
      attribute? :badge_url, Types::String.optional

      # @!attribute [r] business_id
      #   @return [String]
      attribute? :business_id, Types::String.optional

      # @!attribute [r] created_at
      #   @return [String]
      attribute? :created_at, Types::String.optional

      # @!attribute [r] updated_at
      #   @return [String]
      attribute? :deleted_at, Types::Nil.optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] is_active
      #   @return [TrueClass,FalseClass]
      attribute? :is_active, Types::Bool.optional

      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] phone_number
      #   @return [String]
      attribute? :phone_number, Types::String.optional

      # @!attribute [r] phone_number_id
      #   @return [String]
      attribute? :phone_number_id, Types::String.optional

      # @!attribute [r] updated_at
      #   @return [String]
      attribute? :updated_at, Types::String.optional

      # @!attribute [r] use_channel_responder
      #   @return [TrueClass,FalseClass]
      attribute? :use_channel_responder, Types::Bool.optional

      # @!attribute [r] webhook_identifier
      #   @return [String]
      attribute? :webhook_identifier, Types::String.optional
    end
  end
end
