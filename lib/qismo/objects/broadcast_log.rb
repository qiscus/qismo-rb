module Qismo
  module Objects
    # Broadcast log object
    #
    class BroadcastLog < Qismo::Object
      # @!attribute [r] button_params
      #   @return [String]
      attribute? :button_params, Types::String.optional

      # @!attribute [r] customer_name
      #   @return [String]
      attribute? :customer_name, Types::String.optional

      # @!attribute [r] header_value
      #   @return [String]
      attribute? :header_value, Types::String.optional

      # @!attribute [r] id
      #   @return [Integer]
      attribute? :id, Types::Int.optional

      # @!attribute [r] message
      #   @return [String]
      attribute? :message, Types::String.optional

      # @!attribute [r] message_id
      #   @return [String]
      attribute? :message_id, Types::String.optional

      # @!attribute [r] notes
      #   @return [String]
      attribute? :notes, Types::String.optional

      # @!attribute [r] params
      #   @return [String]
      attribute? :params, Types::String.optional

      # @!attribute [r] phone_number
      #   @return [String]
      attribute? :phone_number, Types::String.optional

      # @!attribute [r] sent_at
      #   @return [String]
      attribute? :sent_at, Types::String.optional

      # @!attribute [r] status
      #   @return [String]
      attribute? :status, Types::String.optional

      # @!attribute [r] template_name
      #   @return [String]
      attribute? :template_name, Types::String.optional

      # @!attribute [r] variables
      #   @return [String]
      attribute? :variables, Types::String.optional
    end
  end
end
