module Qismo
  module Objects
    class AdminProfile < Qismo::Object
      # @!attribute [r] name
      #   @return [String]
      attribute? :name, Types::String.optional

      # @!attribute [r] avatar_url
      #   @return [String]
      attribute? :avatar_url, Types::String.optional

      # @!attribute [r] company_name
      #   @return [String]
      attribute? :company_name, Types::String.optional

      # @!attribute [r] address
      #   @return [String]
      attribute? :address, Types::String.optional

      # @!attribute [r] phone_number
      #   @return [String]
      attribute? :phone_number, Types::String.optional

      # @!attribute [r] industry
      #   @return [String]
      attribute? :industry, Types::String.optional

      # @!attribute [r] email_address
      #   @return [String]
      attribute? :email_address, Types::String.optional

      # @!attribute [r] type
      #   @return [Integer]
      attribute? :type, Types::Int.optional
    end
  end
end
