module Qismo
  module Objects
    # Room additional info object
    #
    class RoomAdditionalInfo < Qismo::Object
      # @!attribute [r] key
      #   @return [String]
      attribute? :key, Types::String.optional

      # @!attribute [r] value
      #   @return [String,Integer,TrueClass,FalseClass]
      attribute? :value, (Types::String.optional | Types::Int.optional | Types::Params::Bool.optional)
    end
  end
end
