module Qismo
  module Objects
    # Custom channel message response object
    #
    class CustomChannelMessageResponse < Qismo::Object
      # @!attribute [r] agent
      #   @return [Qismo::Objects::User]
      attribute? :agent, Qismo::Objects::User.optional

      # @!attribute [r] agent_service
      #   @return [Qismo::Objects::AgentService]
      attribute? :agent_service, Qismo::Objects::AgentService.optional

      # @!attribute [r] room_log
      #   @return [Qismo::Objects::CustomerRoom]
      attribute? :room_log, Qismo::Objects::CustomerRoom
    end
  end
end
