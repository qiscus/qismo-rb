module Qismo
  module Objects
    # WA credit info object
    #
    class WaCreditInfo < Qismo::Object
      # Wa credit info channel object
      #
      class Channel < Qismo::Object
        # WA last payment object
        #
        class LastPayment < Qismo::Object
          # @!attribute [r] credit
          #   @return [String]
          attribute? :credit, Types::String.optional

          # @!attribute [r] due_date
          #   @return [String]
          attribute? :due_date, Types::String.optional

          # @!attribute [r] id
          #   @return [Integer]
          attribute? :id, Types::Int.optional

          # @!attribute [r] issued_date
          #   @return [String]
          attribute? :issued_date, Types::String.optional

          # @!attribute [r] status
          #   @return [Integer]
          attribute? :status, Types::Int.optional

          # @!attribute [r] status_string
          #   @return [String]
          attribute? :status_string, Types::String.optional

          # @!attribute [r] updated_at
          #   @return [String]
          attribute? :updated_at, Types::String.optional
        end

        # @!attribute [r] balance
        #   @return [Integer]
        attribute? :balance, Types::Int.optional

        # @!attribute [r] id
        #   @return [Integer]
        attribute? :id, Types::Int.optional

        # @!attribute [r] is_bsp_qiscus
        #   @return [TrueClass,FalseClass]
        attribute? :is_bsp_qiscus, Types::Bool.optional

        # @!attribute [r] last_payment
        #   @return [Qismo::Objects::WaCreditInfo::Channel]
        attribute? :last_payment, LastPayment.optional

        # @!attribute [r] name
        #   @return [String]
        attribute? :name, Types::String.optional
      end

      # @!attribute [r] channels
      #   @return [Array<Qismo::Objects::WaCreditInfo::Channel>]
      attribute? :channels, Types.Array(Channel.optional).optional

      # @!attribute [r] free_sessions
      #   @return [Integer]
      attribute? :free_sessions, Types::Int.optional
    end
  end
end
