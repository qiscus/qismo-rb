module Qismo
  module Objects
    # Office hour object
    #
    class OfficeHour < Qismo::Object
      # Office hour element object
      #
      class OfficeHourElement < Qismo::Object
        # @!attribute [r] day
        #   @return [Integer]
        attribute? :day, Types::Int.optional

        # @!attribute [r] starttime
        #   @return [String]
        attribute? :starttime, Types::String.optional

        # @!attribute [r] endtime
        #   @return [String]
        attribute? :endtime, Types::String.optional
      end

      # @!attribute [r] online_message
      #   @return [String]
      attribute? :online_message, Types::String.optional

      # @!attribute [r] offline_message
      #   @return [String]
      attribute? :offline_message, Types::String.optional

      # @!attribute [r] timezone
      #   @return [String]
      attribute? :timezone, Types::String.optional

      # @!attribute [r] send_online_if_resolved
      #   @return [TrueClass,FalseClass]
      attribute? :send_online_if_resolved, Types::Bool.optional

      # @!attribute [r] send_offline_each_message
      #   @return [TrueClass,FalseClass]
      attribute? :send_offline_each_message, Types::Bool.optional

      # @!attribute [r] office_hours
      #   @return [Array<Qismo::Objects::OfficeHour::OfficeHourElement>]
      attribute? :office_hours, Types.Array(OfficeHourElement.optional).optional

      def in_office_hour?
        return false if timezone.blank?

        offset = ::Time.zone_offset(timezone)
        tz_current = ::Time.current.getlocal(offset)

        today_office_hour = office_hours.find { |oh| oh.day == tz_current.strftime("%u").to_i }
        return false if today_office_hour.nil?

        start_hour = today_office_hour.starttime
        end_hour = today_office_hour.endtime

        # rubocop:disable Layout/LineLength
        start_time = ::Time.parse("#{tz_current.year}-#{tz_current.month}-#{tz_current.day} #{start_hour} #{timezone}")
        end_time = ::Time.parse("#{tz_current.year}-#{tz_current.month}-#{tz_current.day} #{end_hour} #{timezone}")
        # rubocop:enable Layout/LineLength

        tz_current.between?(start_time, end_time)
      end
    end
  end
end
