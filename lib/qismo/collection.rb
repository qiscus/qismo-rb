module Qismo
  # Class to handle paginated response from API
  #
  class Collection < Array
    # Collection object
    #
    # @param data [Array]
    # @param prev_page [String,Integer]
    # @param next_page [String,Integer]
    # @param pagination_type [String]
    def initialize(data, prev_page: nil, next_page: nil)
      super(data)

      @prev_page = prev_page
      @next_page = next_page
    end

    attr_reader :next_page
    attr_reader :prev_page
  end
end
