module Qismo
  # Objectified hash interface for accesing hash using dot notation
  #
  class ObjectifiedHash < Hash
    # method missing patch
    def method_missing(method, *args, &block)
      return super unless respond_to?(method)

      fetch(method.to_s)
    end

    # respond to patch
    def respond_to?(method_name, include_private = false)
      fetch(method_name.to_s)

      true
    rescue KeyError
      super(method_name, include_private)
    end

    # respond to missing patch
    def respond_to_missing?(method_name, include_private = false)
      fetch(method_name.to_s)

      true
    rescue KeyError
      super(method_name, include_private)
    end
  end
end
