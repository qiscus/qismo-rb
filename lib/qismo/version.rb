# frozen_string_literal: true

module Qismo
  # @return [String]
  VERSION = "0.18.3"
end
