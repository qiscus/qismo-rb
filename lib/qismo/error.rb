# frozen_string_literal: true

module Qismo
  # Qismo ruby base error
  #
  class Error < StandardError
    attr_reader :message
  end

  # Http timeout error
  #
  class HTTPTimeoutError < Error
  end

  # Http request error
  #
  class HTTPRequestError < Error
    attr_reader :message, :status_code, :response_body

    # Initiate error
    #
    # @param message [String]
    # @param status_code [Integer]
    # @param response_body [String]
    def initialize(message, status_code:, response_body:)
      super(message.to_s)

      @message = message
      @status_code = status_code
      @response_body = response_body
    end
  end

  # Http internal server error
  #
  class InternalServerError < HTTPRequestError
  end

  # Http 400 bad request error
  #
  class BadRequestError < HTTPRequestError
  end

  # Http 401 unauthorized error
  #
  class UnauthorizedError < HTTPRequestError
  end

  # Http 402 payment required error
  #
  class PaymentRequiredError < HTTPRequestError
  end

  # Http 403 forbidden error
  #
  class ForbiddenError < HTTPRequestError
  end

  # Http 404 not found error
  #
  class NotFoundError < HTTPRequestError
  end

  # Http 429 too many requests error
  #
  class TooManyRequestError < HTTPRequestError
  end
end
