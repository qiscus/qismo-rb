require "dry-struct"

module Qismo
  # Object interface
  #
  class Object < Dry::Struct
    include Qismo::Types

    transform_keys(&:to_sym)

    class << self
      # Create object from array of hash
      #
      # @param array [Array<Hash>]
      # @return [Array<Object>]
      def from_array(array)
        array.map { |a| new(a) }
      end
    end
  end
end
