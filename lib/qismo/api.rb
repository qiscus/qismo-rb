# frozen_string_literal: true

module Qismo
  # API wrappers
  #
  module Api
    # @!parse include Qismo::Client

    def get_profile
      body = get("/api/v1/admin/get_profile")
      Qismo::Objects::AdminProfile.new(body.data)
    end

    # List customer rooms
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#704a0c40-56b2-4a94-9c1d-e1529923cbb6
    # @param channels [Array<Hash>]
    #   Filter rooms by channels. Example: [{ source: "wa", channel_id: 716171 }]
    # @param status [String]
    #   Filter rooms by status. Valid values are "resolved", "unresolved", "expired",or "almost_expired"
    # @param serve_status [String]
    #   Filter rooms by serve status. Valid values are "served" or "unserved". By default, we will retrieve all serve_status
    # @param name [String]
    #   Filter rooms by customer nam
    # @param limit [Integer]
    #   Limit the number of rooms returned in one page. By default, it will return 50 rooms data
    # @param tag_ids [Array<Integer>]
    #   Filter rooms by its tag
    # @param user_ids [Array<Integer>]
    #   Filter rooms by the agent who handled the rooms
    # @param order [String]
    #   Order returned data by the timestamp. By default, we will return the newest rooms first
    # @param cursor_after [String]
    #   Next page cursor. If you are on last page, the cursor returned will be nil
    # @param cursor_before [String]
    #   Previous page cursor. If you are on first page, the cursor returned will be nil
    # @param is_handled_by_bot [TrueClass,FalseClass]
    #   Filter rooms by chatbot activation status in room
    # @return [Qismo::Collection<Qismo::CustomerRoom>]
    def list_rooms(channels: nil, status: nil, serve_status: nil, name: nil, limit: 50, tag_ids: nil, user_ids: nil, order: "desc", cursor_after: nil, cursor_before: nil, is_handled_by_bot: nil)
      body = post("/api/v2/customer_rooms", {
        channels: channels,
        status: status,
        serve_status: serve_status,
        name: name,
        limit: limit,
        tag_ids: tag_ids,
        user_ids: user_ids,
        order: order,
        cursor_after: cursor_after,
        cursor_before: cursor_before,
        is_handled_by_bot: is_handled_by_bot
      })

      Collection.new(
        Qismo::Objects::CustomerRoom.from_array(body.data.customer_rooms),
        next_page: body.meta.cursor_after,
        prev_page: body.meta.cursor_before
      )
    end

    # Get room by id
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#8c803377-eea2-4879-9d66-8906d9f41275
    # @param room_id [Integer]
    # @return [Qismo::Objects::CustomerRoom]
    def get_room(room_id:)
      body = get("/api/v2/customer_rooms/#{room_id}")
      if body.data.customer_room.nil?
        raise Qismo::NotFoundError.new("Room not found", status_code: 404, response_body: body.to_json)
      end

      Qismo::Objects::CustomerRoom.new(body.data.customer_room)
    end

    # List tags inside room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#d40b54a7-2b37-4afc-b474-62593001274e
    # @param room_id [Integer]
    # @return [Qismo::Collection<Qismo::Objects::Tag>]
    def list_room_tags(room_id:)
      Qismo::Collection.new(
        Qismo::Objects::Tag.from_array(
          get("/api/v2/room_tags/#{room_id}").data
        )
      )
    end

    # Add room tag
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#9737d580-d156-4213-85a8-95fb3f1ad964
    # @param room_id [Integer]
    # @param tag [String]
    #   Tag name
    # @return [Qismo::Objects::Tag]
    def add_room_tag(room_id:, tag:)
      Qismo::Objects::Tag.new(
        post("/api/v2/room_tags/#{room_id}", tag: tag).data
      )
    end

    # Delete room tag
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#f4d173a1-3fb1-4151-87fd-106982bcc4a2
    # @param room_id [Integer]
    # @param tag_id [Integer]
    # @return [TrueClass]
    def delete_room_tag(room_id:, tag_id:)
      delete("/api/v2/room_tags/#{room_id}/#{tag_id}")
      true
    end

    # List room additional info
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#88c2287b-21af-4afd-b495-aaaa2818c381
    # @param room_id [Integer]
    # @return [Array<Qismo::Objects::RoomAdditionalInfo>]
    def list_room_info(room_id:)
      Qismo::Objects::RoomAdditionalInfo.from_array(
        get("/api/v1/qiscus/room/#{room_id}/user_info").data.extras.user_properties
      )
    rescue NoMethodError
      Qismo::Objects::RoomAdditionalInfo.from_array([])
    end

    # Set room additional info
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#2b968e9e-2a76-4569-8763-f883e11dc5a7
    # @note This will replace your current room additional info
    # @param room_id [Integer]
    # @param info [Array<Hash>]
    #   Key value pair of additional info. Ex: [{ key: "Ticket Link", value: "https://ticket.com" }]
    # @return [Array<Qismo::Objects::RoomAdditionalInfo>]
    def create_room_info(room_id:, info:)
      Qismo::Objects::RoomAdditionalInfo.from_array(
        post(
          "/api/v1/qiscus/room/#{room_id}/user_info",
          user_properties: info
        ).data.extras.user_properties
      )
    end

    # Update room additional info
    #
    # @param room_id [Integer]
    # @param info [Array<Hash>]
    #   Key value pair of additional info. Ex: [{ key: "Ticket Link", value: "https://ticket.com" }]
    # @return [Array<Qismo::Objects::RoomAdditionalInfo>]
    def update_room_info(room_id:, info:)
      old_info = room_additional_info(room_id).as_json
      new_info = (info.as_json + old_info).uniq { |h| h.values_at("key") }
      set_room_additional_info(room_id: room_id, info: new_info)
    end

    # List broadcast history inside room
    #
    # @param room_id [Integer]
    # @param page [Integer]
    # @param limit [Integer]
    # @return [Qismo::Collection<Qismo::Objects::BroadcastLog>]
    def list_room_broadcasts(room_id:, page: 1, limit: 25)
      body = get("/api/v2/customer_rooms/#{room_id}/broadcast_history", {
        page: page,
        limit: limit
      })

      Qismo::Collection.new(
        Qismo::Objects::BroadcastLog.from_array(body.data.broadcast_logs),
        next_page: ((body.meta.page < body.dig("meta", "total_page")) ? (body.meta.page + 1) : nil),
        prev_page: ((body.meta.page > 1) ? (body.meta.page - 1) : nil)
      )
    end

    # Assign an agent to a room
    #
    # @param room_id [Integer]
    # @param agent_id [Integer]
    # @param replace_latest_agent [TrueClass,FalseClass]
    #   Replace agents in room or not. Don't combine this param
    #   with :max_agent param
    # @param max_agent [Integer]
    #   Specify max agent that can be assigned to a room. For
    #   example, if there are 1 agent in a room and you specific
    #   max_agent to 1, you will get bad request error. If you set
    #   param :replace_latest_agent to true, this param will be forced
    #   to nil, which no be sent on http request
    # @return [Qismo::Objects::User]
    def assign_agent(room_id:, agent_id:, replace_latest_agent: false, max_agent: nil)
      Qismo::Objects::User.new(
        post("/api/v1/admin/service/assign_agent", {
          room_id: room_id.to_s,
          agent_id: agent_id,
          replace_latest_agent: replace_latest_agent,
          max_agent: (replace_latest_agent == true) ? nil : max_agent
        }).data.assign_agent
      )
    end

    # Remove agent from room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#7a67e214-28e5-47c1-b4ee-7246977dfcff
    # @param room_id [Integer]
    # @param agent_id [Integer]
    # @return [TrueClass]
    def remove_agent(room_id:, agent_id:)
      post(
        "/api/v1/admin/service/remove_agent", {
          room_id: room_id.to_s,
          agent_id: agent_id
        }
      )

      true
    end

    # Resolve room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#a1ff75d0-b637-4b79-a259-fb9d084d1659
    # @param room_id [Integer]
    # @param last_comment_id [Integer]
    #   Id of last message in the room. If you dont specify this param,
    #   we will use current epoc timestamp
    # @param notes [String]
    #   Specify room notes. You can also use simple markdown markup
    #   for this param
    # @param is_send_email [TrueClass,FalseClass]
    #   Send email to customer or no. This param will only worked if
    #   you are using valid email as customer identifier
    # @param extras [Hash]
    #   Room extras in valid hash
    # @return [Qismo::Objects::AgentService]
    def resolve_room(room_id:, last_comment_id: Time.now.to_i, notes: nil, is_send_email: false, extras: nil)
      Qismo::Objects::AgentService.new(
        post("/api/v1/admin/service/mark_as_resolved", {
          room_id: room_id.to_s,
          last_comment_id: last_comment_id,
          notes: notes,
          is_send_email: is_send_email,
          extras: extras
        }).data.service
      )
    end

    # Get agent that can be assigned to room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#a62432c6-1aba-4e2b-be9a-2f7151db9119
    # @param source [String]
    #   Channel source. Available sources are `wa`, `line`, `telegram`,
    #   `qiscus`, `ig`, or `fb`. For custom channel, use that channel's
    #   identifier_key as source
    # @param channel_id [Integer]
    #   The channel id you want to allocate
    # @param ignore_availability [TrueClass,FalseClass]
    #   Ignore agent availability. If you set this param to true, agent
    #   that are unavailable will be considered as allocatable too
    # @return [Qismo::Objects::User]
    def allocate_agent(source:, channel_id:, ignore_availability: false)
      Qismo::Objects::User.new(
        post("/api/v1/admin/service/allocate_agent", {
          source: source,
          channel_id: channel_id,
          ignore_availability: ignore_availability
        }).data.agent
      )
    end

    # Get agent that can be allocated to room and automatically assign them
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#f88465ed-379f-4202-a8a4-e0c3bebcfeab
    # @param room_id [Integer]
    # @param ignore_availability [TrueClass,FalseClass]
    #   Ignore agent availability. If you set this param to true, agent
    #   that are unavailable will be considered as allocatable too
    # @return [Qismo::Objects::User]
    def allocate_and_assign_agent(room_id:, ignore_availability: false)
      Qismo::Objects::User.new(
        post("/api/v1/admin/service/allocate_assign_agent", {
          room_id: room_id.to_s,
          ignore_availability: ignore_availability
        }).data.agent
      )
    end

    # List agents that are not in room and can be assigned to room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#10d75d0c-ebd2-44c6-ab19-a0bafe7ad982
    # @param room_id [Integer]
    # @param search [String]
    # @param limit [Integer]
    # @param cursor_after [String]
    # @param cursor_before [String]
    # @return [Qismo::Collection<Qismo::Objects::User>]
    def list_other_agents(room_id:, search: nil, limit: 15, cursor_after: nil, cursor_before: nil)
      body = get("/api/v2/admin/service/other_agents", {
        room_id: room_id.to_s,
        search: search,
        limit: limit,
        cursor_after: cursor_after,
        cursor_before: cursor_before
      })

      Collection.new(
        Qismo::Objects::User.from_array(body.data.agents),
        next_page: body.meta.cursor_after,
        prev_page: body.meta.cursor_before
      )
    end

    # List available agents in room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#0d5db283-c7f8-42e0-ba02-6cbcbc1d2bb3
    # @param room_id [Integer]
    # @param is_available_in_room [TrueClass,FalseClass]
    # @param search [String]
    # @param limit [Integer]
    # @param cursor_after [String]
    # @param cursor_before [String]
    # @return [Qismo::Collection<Qismo::Objects::User>]
    def list_available_agents(room_id:, is_available_in_room: true, search: nil, limit: 15, cursor_after: nil, cursor_before: nil)
      body = get("/api/v2/admin/service/available_agents", {
        room_id: room_id.to_s,
        is_available_in_room: is_available_in_room,
        search: search,
        limit: limit,
        cursor_after: cursor_after,
        cursor_before: cursor_before
      })

      Collection.new(
        Qismo::Objects::User.from_array(body.data.agents),
        next_page: body.meta.cursor_after,
        prev_page: body.meta.cursor_before
      )
    end

    # Get new session webhook config
    #
    # @return [Qismo::Objects::Webhook]
    def get_new_session_webhook
      config = get("/api/v2/app/config/new_session_webhook").data.configs
      Qismo::Objects::Webhook.new(
        enabled: config.is_new_session_webhook_enabled,
        url: config.new_session_webhook_url
      )
    end

    # Set new session webhook
    #
    # @param url [String]
    # @param enabled [TrueClass,FalseClass]
    # @return [Qismo::Objects::Webhook]
    def create_new_session_webhook(url:, enabled: true)
      config = post("/api/v2/app/config/new_session_webhook", url: url, enabled: enabled).data.configs
      Qismo::Objects::Webhook.new(
        enabled: config.is_new_session_webhook_enabled,
        url: config.new_session_webhook_url
      )
    end

    # Get auth webhook config
    #
    # @return [Qismo::Objects::Webhook]
    def get_auth_webhook
      config = get("/api/v2/app/config/auth_webhook").data.configs
      Qismo::Objects::Webhook.new(
        enabled: config.is_auth_webhook_enabled,
        url: config.auth_webhook_url
      )
    end

    # Set auth webhook
    #
    # @param url [String]
    # @param enabled [TrueClass, FalseClass]
    # @return [Qismo::Objects::Webhook]
    def create_auth_webhook(url:, enabled: true)
      config = post("/api/v2/app/config/auth_webhook", url: url, enabled: enabled).data.configs
      Qismo::Objects::Webhook.new(
        enabled: config.is_auth_webhook_enabled,
        url: config.auth_webhook_url
      )
    end

    # Get resolve webhook config
    #
    # @return [Qismo::Objects::Webhook]
    def get_resolve_webhook
      config = get("/api/v1/app/webhook/mark_as_resolved").data
      Qismo::Objects::Webhook.new(
        enabled: config.is_mark_as_resolved_webhook_enabled,
        url: config.mark_as_resolved_webhook_url
      )
    end

    # Set resolve webhook
    #
    # @param url [String]
    # @param options [Hash]
    # @return [Qismo::Objects::Webhook]
    def create_resolve_webhook(url:, enabled: true)
      config = post("/api/v1/app/webhook/mark_as_resolved", webhook_url: url, is_webhook_enabled: enabled).data
      Qismo::Objects::Webhook.new(
        enabled: config.is_mark_as_resolved_webhook_enabled,
        url: config.mark_as_resolved_webhook_url
      )
    end

    # List agents
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#849205ca-00d9-4356-9fdd-f05bff777b4e
    # @param options [Hash]
    # @return [Qismo::Collection<Qismo::Objects::User>]
    def list_agents(page: 1, limit: 10, search: nil, scope: nil)
      body = get("/api/v2/admin/agents", {
        page: page,
        limit: limit,
        search: search,
        scope: scope
      })

      total_page = (body.meta.total_count.to_f / body.meta.per_page.to_f).ceil

      Qismo::Collection.new(
        Qismo::Objects::User.from_array(body.data.agents),
        next_page: ((page < total_page) ? (page + 1) : nil),
        prev_page: ((page > 1) ? (page - 1) : nil)
      )
    end

    # List agents by ids
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#3db6c8c8-8ffe-4a88-b630-41f9d5b62298
    # @param agent_ids [Array<Integer>]
    # @return [Qismo::Collection<Qismo::Objects::User>]
    def list_agents_by_ids(agent_ids:)
      Qismo::Collection.new(
        Qismo::Objects::User.from_array(get("/api/v1/admin/agents/get_by_ids", {"ids[]": agent_ids}).data)
      )
    end

    # List agents by divisions
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#94eeb9cb-dd70-4baf-9f63-361e7922299a
    # @param division_ids [Array<Integer>]
    # @param page [Integer]
    #   Page number
    # @param limit [Integer]
    #   Number of data returned in one page
    # @param is_available [TrueClass,FalseClass]
    #   Filter only available or unavailable agents
    # @param sort [String]
    #   Sort result by descending or ascending
    # @return [Qismo::Collection<Qismo::Objects::User>]
    def list_agents_by_divisions(division_ids, page: 1, limit: 10, is_available: nil, sort: "asc")
      body = get("/api/v2/admin/agents/by_division", {
        "division_ids[]": division_ids,
        page: page,
        limit: limit,
        is_available: is_available,
        sort: sort
      })

      Qismo::Collection.new(
        Qismo::Objects::User.from_array(body.data),
        next_page: ((body.meta.page < body.meta.total_page) ? (body.meta.page + 1) : nil),
        prev_page: ((body.meta.page > 1) ? (body.meta.page - 1) : nil)
      )
    end

    # Get agent by id
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#c6184a6b-ba4d-4f3e-a4da-c6d0fa4597af
    # @param agent_id [Integer]
    # @return [Qismo::Objects::User]
    def get_agent(agent_id:)
      Qismo::Objects::User.new(get("/api/v2/admin/agent/#{agent_id}").data.agent)
    end

    # Get office hour config
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#6f3f0cb0-a391-4945-b01a-95ce81138913
    # @return [Qismo::Objects::OfficeHour]
    def get_office_hours
      Qismo::Objects::OfficeHour.new(
        get("/api/v1/admin/office_hours").data
      )
    end

    # List WA broadcast templates
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#e38d2244-9559-4015-99d5-8c707f6c01bd
    # @param page [Integer]
    #   Page number
    # @param limit [Integer]
    #   Number of data returned in one page
    # @param approved [TrueClass,FalseClass]
    #   Filter template by approval status
    # @return [Qismo::Collection<Qismo::Objects::HsmTemplate>]
    def list_wa_broadcast_templates(page: 1, limit: 10, approved: nil)
      body = get("/api/v3/admin/hsm", {
        page: page,
        limit: limit,
        approved: approved
      })

      Qismo::Collection.new(
        Qismo::Objects::HsmTemplate.from_array(body.data.hsm_templates),
        next_page: ((body.meta.page < body.meta.total_page) ? (body.meta.page + 1) : nil),
        prev_page: ((body.meta.page > 1) ? (body.meta.page - 1) : nil)
      )
    end

    # Get Whatsapp channel credit info
    #
    # @return [Qismo::Objects::WaCreditInfo]
    def get_wa_credit
      Qismo::Objects::WaCreditInfo.new(
        get("/api/v2/admin/wa_pricing/balance").data
      )
    end

    # Send WA outbound message
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#d0183cf6-deca-439b-aff3-2e2f007c15a9
    # @param phone_number [String]
    # @param channel_id [Integer]
    # @param template_name [String]
    # @param namespace [String]
    # @param language [String]
    # @param template_detail_id [Integer]
    # @param variables [Array<String>]
    # @param button_params [Array<Hash>]
    # @param header_value [Hash]
    # @return [Qismo::Objects::BroadcastJob]
    def send_wa_outbound(
      phone_number:,
      template_detail_id: nil,
      channel_id: nil,
      template_name: nil,
      namespace: nil,
      language: nil,
      variables: [],
      button_params: nil,
      header_value: nil
    )
      if template_detail_id.nil?
        raise ArgumentError, ":channel_id is required if you dont use :template_detail_id" if channel_id.nil?
        raise ArgumentError, ":template_name is required if you dont use :template_detail_id" if template_name.nil?
        raise ArgumentError, ":namespace is required if you dont use :template_detail_id" if namespace.nil?
        raise ArgumentError, ":language is required if you dont use :template_detail_id" if language.nil?
      end

      body = post("/api/v3/admin/broadcast/client", {
        phone_number: phone_number,
        template_detail_id: template_detail_id,
        channel_id: channel_id,
        template_name: template_name,
        namespace: namespace,
        language: language,
        variables: variables,
        button_params: button_params,
        header_value: header_value
      })

      Qismo::Objects::BroadcastJob.new(body.data)
    end

    # Upload wa broadcast file that want to be sent in broadcast
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#030acde2-21cf-4373-8d11-075206053c1d
    # @param file [HTTP::FormData]
    # @param template_detail_id [Integer]
    # @param separator [String]
    # @return [Integer]
    def upload_wa_broadcast_csv(file:, template_detail_id:, separator: ",")
      raise ArgumentError, "Invalid file" unless file.is_a?(HTTP::FormData::File)

      body = post_upload("/api/v3/admin/broadcast/upload_csv", {
        file: file,
        template_detail_id: template_detail_id,
        separator: separator
      })

      body.broadcast_file_id
    end

    # Send WA broadcast
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#717c1a12-facb-4945-aed5-04557696b873
    # @param file [HTTP::FormData::File,Integer]
    # @param template_detail_id [Integer]
    # @param name [String]
    # @param separator [String]
    # @param started_at [Time]
    # @return [Qismo::Objects::BroadcastJob]
    def send_wa_broadcast(file:, template_detail_id:, name: nil, separator: ",", started_at: nil)
      if name.blank?
        name = default_broadcast_name
      end

      if started_at.present? && !started_at.is_a?(Time)
        unless started_at.is_a?(Time)
          raise ArgumentError, "You must past :Time class for this parameter"
        end

        unless started_at.utc_offset == 0
          raise ArgumentError, "You must set your timezone to UTC"
        end
      end

      file_id = if file.is_a?(HTTP::FormData::File)
        upload_wa_broadcast_csv(file, template_detail_id, separator: separator)
      else
        file.to_i
      end

      Qismo::Objects::BroadcastJob.new(
        post("/api/v3/admin/broadcast/send_broadcast", {
          broadcast_file_id: file_id,
          name: name,
          separator: separator,
          started_at: started_at,
          template_detail_id: template_detail_id
        }).data.broadcast_job
      )
    end

    # List WA broadcast jobs
    #
    # @param page [Integer]
    # @param limit [Integer]
    # @param cursor_after [Integer]
    # @param cursor_before [Integer]
    # @param name [String]
    # @return [Qismo::Collection<Qismo::Objects::BroadcastJob>]
    def list_wa_broadcast_jobs(limit: 10, cursor_after: nil, cursor_before: nil, name: nil)
      body = get("/api/v2/admin/broadcast_jobs", {
        limit: limit,
        cursor_after: cursor_after,
        cursor_before: cursor_before,
        name: name
      })

      Qismo::Collection.new(
        Qismo::Objects::BroadcastJob.from_array(body.data.broadcast_jobs),
        next_page: body.meta.cursor_after,
        prev_page: body.meta.cursor_before
      )
    end

    # Get wa broadcast job by id
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#ed0806c8-2e4a-4ea4-8acb-45c84c63c2da
    # @param broadcast_job_id [Integer]
    # @return [Qismo::Objects::BroadcastJob]
    def get_wa_broadcast_job(broadcast_job_id:)
      Qismo::Objects::BroadcastJob.new(
        get("/api/v2/admin/broadcast_jobs/#{broadcast_job_id}").data.broadcast_job
      )
    end

    # List wa broadcast logs
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#3f2d8ba8-ab14-43b2-af82-74c8b766216f
    # @param broadcast_job_id [Integer]
    # @param options [Hash]
    # @return [Qismo::Collection<Qismo::Objects::BroadcastLog>]
    def list_wa_broadcast_logs(broadcast_job_id:, page: 1, limit: 10)
      body = get("/api/v2/admin/broadcast_logs/#{broadcast_job_id}", {
        page: page,
        limit: limit
      })

      prev_page = (body.meta.page > 1) ? (body.meta.meta - 1) : nil
      next_page = (body.meta.page < body.meta.total_page) ? (body.meta.page + 1) : nil

      Qismo::Collection.new(
        Qismo::Objects::BroadcastLog.from_array(body.data.broadcast_logs),
        next_page: next_page,
        prev_page: prev_page
      )
    end

    # Send message as bot
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#bb77c653-5daa-4e1c-a866-43bca7c494fc
    # @param room_id [Integer]
    # @param message [String]
    # @param type [String]
    # @param payload [Hash]
    # @param extras [Hash]
    # @return [TrueClass]
    def send_bot_message(room_id:, message:, type: "text", payload: {}, extras: {})
      body = post("/#{app_id}/bot", {
        sender_email: admin_email,
        room_id: room_id.to_s,
        message: message,
        type: type,
        payload: payload,
        extras: extras
      })

      if body != "ok"
        raise Qismo::BadRequestError.new(body.to_s, status_code: 400, response_body: body.to_s)
      end

      true
    end

    # Enable chabot in room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#494d825f-a49c-4b18-954e-eaaccb738bcd
    # @param room_id [Integer]
    # @return [TrueClass]
    def enable_bot_in_room(room_id:)
      post("/bot/#{room_id}/activate", is_active: true)
      true
    end

    # Disable chatbot in room
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#494d825f-a49c-4b18-954e-eaaccb738bcd
    # @param room_id [Integer]
    # @return [TrueClass]
    def disable_bot_in_room(room_id:)
      post("/bot/#{room_id}/activate", is_active: false)
      true
    end

    # Handover room from chatbot to human agent
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#40867e85-7412-4e0d-84bd-e2506df23df8
    # @param room_id [Integer]
    # @return [TrueClass]
    def handover_room_from_bot(room_id:)
      request(:post, "/#{app_id}/bot/#{room_id}/hand_over", headers: {
        Authorization: secret_key
      })

      true
    end

    # Handover room from chatbot to human agent in specific divisions
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#8218db08-9753-4d74-ae5f-0ee62f8579b9# <Description>
    #
    # @param room_id [Integer]
    # @param roles [Integer,Array<Integer>]
    # @param find_online_agent [TrueClass,FalseClass]
    # @return [TrueClass,FalseClass]
    def handover_room_from_bot_to_division(room_id:, roles:, find_online_agent: true)
      options = {}
      options[:room_id] = room_id.to_s
      options[:find_online_agent] = find_online_agent

      if roles.is_a?(Array)
        options[:roles] = roles
      else
        options[:role] = roles
      end

      request(
        :post,
        "/#{app_id}/bot/#{room_id}/hand_over_to_role",
        headers: {Authorization: secret_key},
        json: options
      )

      true
    end

    # Initiate chat using Qiscus widget channel
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#d5a555de-59e0-4705-9582-c216d79e9390
    # @param user_id [String]
    # @param name [String]
    # @param nonce [String]
    # @param channel_id [Integer]
    # @param extras [Hash]
    # @param reset_extras [TrueClass,FalseClass]
    # @param room_badge [String]
    # @param avatar [String]
    # @param allocate_agent [TrueClass,FalseClass]
    # @param user_properties [Hash]
    # @param sdk_user_extras [Hash]
    # @param timezone_offset [String]
    # @param notes [String]
    # @param phone_number [String]
    # @param email [String]
    # @return [Qismo::Objects::CustomerRoom]
    def initiate_widget_chat(user_id:, name:, nonce: nil, channel_id: nil, extras: nil, reset_extras: false, room_badge: nil, avatar: nil, allocate_agent: nil, user_properties: nil, sdk_user_extras: nil, timezone_offset: nil, notes: nil, phone_number: nil, email: nil)
      options = {
        user_id: user_id,
        name: name,
        nonce: nonce,
        channel_id: channel_id,
        extras: extras,
        reset_extras: reset_extras,
        room_badge: room_badge,
        avatar: avatar,
        allocate_agent: allocate_agent,
        user_properties: user_properties,
        sdk_user_extras: sdk_user_extras,
        timezone_offset: timezone_offset,
        notes: notes,
        phone_number: phone_number,
        email: email
      }.compact

      options[:app_id] = app_id

      Qismo::Objects::CustomerRoom.new(post("/api/v2/qiscus/initiate_chat", options).data.customer_room)
    end

    # Send messsage to custom channel
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#aee54b21-68f1-4d31-9d81-d3c73b3e125b
    # @param identifier_key [String]
    # @param user_id [String]
    # @param name [String]
    # @param message [String]
    # @param avatar [String]
    # @param type [String]
    # @param payload [Hash]
    # @param extras [Hash]
    # @return [Qismo::Objects::CustomChannelMessageResponse]
    def send_message_to_custom_channel(
      identifier_key:,
      user_id:,
      name:,
      message:,
      avatar: nil,
      type: "text",
      payload: {},
      extras: {}
    )
      Qismo::Objects::CustomChannelMessageResponse.new(
        post("/#{app_id}/api/v2/custom_channel/send", {
          identifier_key: identifier_key,
          user_id: user_id,
          name: name,
          message: message,
          avatar: avatar,
          type: type,
          payload: payload,
          extras: extras
        }).data
      )
    end

    # List integrated channels
    #
    # @see https://documenter.getpostman.com/view/8259884/TVsuCSeT#aee54b21-68f1-4d31-9d81-d3c73b3e125b
    # @return [Qismo::Objects::ListChannelsResponse]
    def list_channels
      Qismo::Objects::ListChannelsResponse.new(
        get("/api/v2/channels").data
      )
    end
  end
end
