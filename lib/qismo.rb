# frozen_string_literal: true

require "pathname"
require "active_support/all"
require "dry-struct"
require "http"

require "qismo/version"
require "qismo/types"
require "qismo/error"

require "qismo/object"
require "qismo/objectified_hash"
require "qismo/collection"

require "qismo/objects/admin_profile"
require "qismo/objects/customer_room"
require "qismo/objects/tag"
require "qismo/objects/room_additional_info"
require "qismo/objects/broadcast_log"
require "qismo/objects/user"
require "qismo/objects/agent_service"
require "qismo/objects/webhook"
require "qismo/objects/office_hour"
require "qismo/objects/hsm_template"
require "qismo/objects/wa_credit_info"
require "qismo/objects/broadcast_job"
require "qismo/objects/custom_channel_message_response"
require "qismo/objects/custom_channel"
require "qismo/objects/fb_channel"
require "qismo/objects/ig_channel"
require "qismo/objects/line_channel"
require "qismo/objects/qiscus_channel"
require "qismo/objects/telegram_channel"
require "qismo/objects/wa_channel"
require "qismo/objects/waca_channel"
require "qismo/objects/list_channels_response"

require "qismo/webhook_requests/on_agent_allocation_needed"
require "qismo/webhook_requests/on_custom_button_clicked"
require "qismo/webhook_requests/on_custom_channel_message_sent"
require "qismo/webhook_requests/on_message_for_bot_sent"
require "qismo/webhook_requests/on_new_session_initiated"
require "qismo/webhook_requests/on_room_resolved"

require "qismo/api"
require "qismo/client"

require "railtie" if defined?(Rails)

# Qismo ruby root module
#
module Qismo
end
