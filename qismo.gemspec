# frozen_string_literal: true

$LOAD_PATH.unshift(::File.join(::File.dirname(__FILE__), "lib"))

require_relative "lib/qismo/version"

Gem::Specification.new do |spec|
  spec.name = "qismo"
  spec.version = Qismo::VERSION
  spec.required_ruby_version = ">= 2.6.0"
  spec.author = "Qiscus Integration"
  spec.email = ["developer@qiscus.net"]
  spec.description = "Ruby wrapper for Qiscus Omnichannel public API"
  spec.summary = spec.description
  spec.homepage = "https://bitbucket.org/qiscus/qismo-rb"
  spec.license = "MIT"

  spec.metadata = {
    "source_code_uri" => "https://bitbucket.org/qiscus/qismo-rb/src/v#{spec.version}",
    "documentation_uri" => "https://www.rubydoc.info/gems/qismo",
    "homepage_uri" => spec.homepage,
    "rubygems_mfa_required" => "true"
  }

  ignored = Regexp.union(
    /\A\.editorconfig/,
    /\A\.byebug_history/,
    /\A\.solargraph/,
    /\A\.ruby-version/,
    /\A\.rspec/,
    /\A\.git/,
    /\A\.rubocop/,
    /\A\.travis.yml/,
    /\A\.vscode/,
    /\Atest/,
    /\Aspec/,
    /\Aexamples/,
    /\Abin/,
    /\Afeatures/
  )

  spec.files = `git ls-files`.split("\n").reject { |f| ignored.match(f) }
  spec.require_paths = ["lib"]

  spec.add_runtime_dependency("activesupport")
  spec.add_runtime_dependency("http")
  spec.add_runtime_dependency("dry-struct", "~> 1.6")
end
